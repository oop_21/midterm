package com.chaiwat.midterm;

public class Map {
    private int width;
    private int height;
    private Unit units[];
    private int unitCont;
    public boolean w;
    private int xp;
    private int yp;
    private int amr = 2;

    public Map(int width, int height) {
        this.width = width;
        this.height = height;
        this.units = new Unit[width * height];
        this.unitCont = 0;
    }

    public Map() {
        this(100, 100);
    }

    public void print() {
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                printBlock(x, y);
            }
            System.out.println();
        }
    }

    private void printBlock(int x, int y) {
        for (int i = 0; i < unitCont; i++) {
            Unit unit = this.units[i];
            if (unit.isOn(x, y)) {
                System.out.print(unit.getSymbol());
                return;
            }
        }
        System.out.print("-");
    }

    public String toString() {
        return "Map(" + this.width + "," + this.height + ")";
    }

    public void add(Unit unit) {
        if (unitCont == (width * height))
            return;
        this.units[unitCont] = unit;
        unitCont++;
    }

    public boolean printUnits() {
        for (int i = 0; i < unitCont; i++) {
            System.out.println(this.units[i]);
            this.w = true;
        }
        return true;
    }

    public boolean isOn(int x, int y) {
        return isInWidth(x) && isInHeight(y);
    }

    public boolean isInWidth(int x) {
        return x >= 0 && x < width;
    }

    public boolean isInHeight(int y) {
        return y >= 0 && y < height;
    }

    public boolean ReduceStamina() {
        if (w == true) {
        }
        return true;
    }

    public boolean Damage() {

        for (int i = 0; i < unitCont; i++) {
            Unit unit = this.units[i];
            if (unit.isDominate() == false) {
                xp = unit.getX();
                yp = unit.getY();
            }
        }
        for (int i = 0; i < unitCont; i++) {
            Unit unit = this.units[i];
            if (unit.isDominate() == true) {
                if (unit.getX() == xp && unit.getY() == yp) {
                    return true;
                }
            }
        }
        return false;
    }

    public int ArmorZero() {
        if (Damage()) {
            this.amr = amr - 1;
            return amr;
        }
        return amr;
    }
    public boolean EndWin() {

        for (int i = 0; i < unitCont; i++) {
            Unit unit = this.units[i];
            if (unit.isDominate() == false) {
                xp = unit.getX();
                yp = unit.getY();
            }
        }
        for (int i = 0; i < unitCont; i++) {
            Unit unit = this.units[i];
            if (unit.isDominate() == true) {
                if (xp == 19  && yp == 3) {
                    return true;
                }
            }
        }
        return false;
}
}