package com.chaiwat.midterm;

import java.util.Scanner;

import main.java.com.chaiwat.midterm.Armor;
import main.java.com.chaiwat.midterm.End;
import main.java.com.chaiwat.midterm.FinashLine;
import main.java.com.chaiwat.midterm.HP;
import main.java.com.chaiwat.midterm.Stamina;
import main.java.com.chaiwat.midterm.Trap;

public class GameApp {

    static Map map = new Map(20, 4);

    static HP hp = new HP(map, "HP", 1);
    static Armor am = new Armor(map, "Armor", 1);
    static Stamina sta = new Stamina(map, "Stamina", 35);

    static Trap trap1 = new Trap(map, 1, 0);
    static Trap trap2 = new Trap(map, 1, 1);
    static Trap trap3 = new Trap(map, 1, 2);
    static Trap trap4 = new Trap(map, 3, 3);
    static Trap trap5 = new Trap(map, 3, 2);
    static Trap trap6 = new Trap(map, 3, 0);
    static Trap trap7 = new Trap(map, 4, 0);
    static Trap trap8 = new Trap(map, 19, 0);
    static Trap trap9 = new Trap(map, 6, 1);
    static Trap trap10 = new Trap(map, 6, 2);
    static Trap trap11 = new Trap(map, 3, 3);
    static Trap trap12 = new Trap(map, 7, 2);
    static Trap trap13 = new Trap(map, 7, 0);
    static Trap trap14 = new Trap(map, 9, 3);
    static Trap trap15 = new Trap(map, 11, 3);
    static Trap trap16 = new Trap(map, 11, 0);
    static Trap trap17 = new Trap(map, 11, 1);
    static Trap trap18 = new Trap(map, 12, 3);
    static Trap trap19 = new Trap(map, 12, 0);
    static Trap trap20 = new Trap(map, 12, 1);
    static Trap trap21 = new Trap(map, 9, 2);
    static Trap trap22 = new Trap(map, 9, 1);

    static Trap end = new Trap(map, 19, 3);

    static Player Player = new Player(map, 'P', 0, 0);
    // static End end = new End(map, 19, 3);
    static Scanner sc = new Scanner(System.in);

    public static String input() {
        return sc.next();
    }

    public static void process(String command) {
        switch (command) {
            case "w":
                Player.up();
                break;
            case "s":
                Player.down();
                break;
            case "a":
                Player.left();
                break;
            case "d":
                Player.right();
                break;
            case "q":
                System.exit(0);
                break;
            default:
                break;
        }
    }

    public static void main(String[] args) {

        map.add(trap1);
        map.add(trap2);
        map.add(trap3);
        map.add(trap4);
        map.add(trap5);
        map.add(trap6);
        map.add(trap7);
        map.add(trap8);
        map.add(trap7);
        map.add(trap8);
        map.add(trap9);
        map.add(trap10);
        map.add(trap11);
        map.add(trap12);
        map.add(trap13);
        map.add(trap14);
        map.add(trap15);
        map.add(trap16);
        map.add(trap17);
        map.add(trap18);
        map.add(trap19);
        map.add(trap20);
        map.add(trap21);
        map.add(trap22);
        map.add(end);
        map.add(Player);

        while (true) {
            System.out.println("====================");
            System.out.println("     ________");
            System.out.println("    [Resource]");
            System.out.println("             ");
            System.out.println(hp.getName() + " : " + hp.setNum());
            System.out.println(am.getName() + " : " + am.setNum());
            System.out.println(sta.getName() + " : " + sta.setNum());
            System.out.println(" ");
            System.out.println("====================");
            map.print();
            System.out.println("====================");
            String command = input();
            process(command);

            if (map.EndWin()) {
                System.out.println("________________");
                System.out.println("  [YOU WIN]");
                System.exit(0);
                break;
            } 
            else if (hp.getNum() == 0) {
                System.out.println("________________");
                System.out.println("  [YOU LOSS]");
                System.exit(0);
                break;
            }
            else if (sta.getNum() <= 0) {
                System.out.println("________________");
                System.out.println("  [YOU LOSS]");
                System.exit(0);
                break;
            }
        }
    }
}
