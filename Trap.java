package main.java.com.chaiwat.midterm;

import com.chaiwat.midterm.Map;
import com.chaiwat.midterm.Unit;

public class Trap extends Unit{
    public Trap(Map map,int x,int y){
        super(map,'X',x , y,true);
    }
}
