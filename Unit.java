package com.chaiwat.midterm;

public class Unit {
    private char symbol;
    private int x;
    private int y;
    private Map map;
    private boolean dominate ;


    public Unit(Map map,char symbol,int x,int y,boolean dominate){
        this.map = map;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
        this.dominate = dominate;
    }
    public boolean isOn(int x,int y){
        return this.x == x && this.y == y;
    }
    public boolean setX(int x){
        this.x = x;
        return true;
    }
    public boolean setY(int y){
        this.y = y;
        return true;
    }
    public boolean setXY(int x,int y){
        this.x = x;
        this.y = y;
        return true;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public boolean isDominate(){
        return dominate;
    }
    public Map getMap(){
        return map;
    }
    public char getSymbol(){
        return symbol;
    }
}
