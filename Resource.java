package main.java.com.chaiwat.midterm;

import com.chaiwat.midterm.Map;

public class Resource {
    private String name;
    protected int num;
    protected Map map;
    public Resource(Map map,String name,int num){
        this.map = map;
        this.name = name;
        this.num = num;
    }
    public int getNum(){
        return num;
    }
    public String getName(){
        return name;
    }

}
